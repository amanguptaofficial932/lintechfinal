import React from 'react'

const Content = () => {
  return (
    <div className='p-7 pb-9' style={{background:"#151515"}}>
          <h1 className="font-extrabold text-white text-9xl p-9  pb-0">WE ARE </h1>
          <div className='flex'><span className='font-extrabold text-white text-2xl mx-6 flex ' style={{alignItems:"flex-end"}}>BORN IN LONDON </span> <h1 className="font-extrabold text-white text-9xl p-9  pb-0">WORTHY </h1>
        
          <p className='font-extrabold text-white text-2xl flex ' style={{alignItems:"flex-end"}}>CLIENTS <br /> ALL AROUND THE GLOBE</p>
          </div>
          
    </div>
  )
}

export default Content