import React from "react";
import EmojiEventsIcon from "@mui/icons-material/EmojiEvents";
import ArrowForwardIcon from "@mui/icons-material/ArrowForward";
import ArrowBackIcon from "@mui/icons-material/ArrowBack";
const Card = () => {
  return (
    <div>
      <div className="p-7 flex justify-between">
        <p className="p-8 text-4xl" style={{ width: "70%" }}>
          We have delivered outstanding results for more than 450 businesses in
          the past 3 years
        </p>

        {/* <p className="p-8">
          <ArrowForwardIcon className="p-5" style={{background:"gray",borderRadius:"50%",fontSize:"35px"}} />
        </p>
        <p className="p-8">
          <ArrowBackIcon />
        </p> */}
      </div>

      <div class="grid grid-cols-4 grid-rows-1 gap-4 p-8 pb-0">
        <div
          className="bg-black p-5 shadow-xl rounded-lg relative cursor-pointer"
          style={{ width: "300px", height: "300px" }}
        >
          <h3 className="text-white text-4xl p-3 font-extrabold">
            10 million+
          </h3>

          <p className="text-white text-1xl absolute bottom-0 p-3 mb-5">
            Marketplace users <br /> growth
          </p>
        </div>

        <div
          className="bg-black p-5 shadow-xl rounded-lg relative cursor-pointer"
          style={{ width: "300px", height: "300px" }}
        >
          <h3 className="text-white text-4xl p-3 font-extrabold">
            420%
          </h3>

          <p className="text-white text-1xl absolute bottom-0 p-3 mb-5">
          Onboarding conversions <br /> increased 
          </p>
        </div>

        <div
          className="bg-black p-5 shadow-xl rounded-lg relative cursor-pointer"
          style={{ width: "300px", height: "300px" }}
        >
          <h3 className="text-white text-4xl p-3 font-extrabold">
            51%
          </h3>

          <p className="text-white text-1xl absolute bottom-0 p-3 mb-5">
          Retention rate  <br /> growth
          </p>
        </div>

        <div
          className="bg-black p-5 shadow-xl rounded-lg relative cursor-pointer"
          style={{ width: "300px", height: "300px" }}
        >
          <h3 className="text-white text-4xl p-3 font-extrabold">
          140%
          </h3>

          <p className="text-white text-1xl absolute bottom-0 p-3 mb-5">
          CRO in SaaS  <br /> improved 
          </p>
        </div>

       

      
        
      </div>

      <div className="flex justify-between relative ">
        <h1 className="font-extrabold text-9xl p-8 ml-5 pb-0">OUR PAST <br /> PROJECTS</h1>
        <div className=" absolute bottom-0 flex justify-end mb-5 right-8  " style={{width:"30%" ,borderBottom:"5px solid black"}}> <p className="font-extrabold ">500+PROJECTS</p></div>
       
      </div>
    </div>
  );
};

export default Card;
