import React from 'react'
import EmojiEventsIcon from '@mui/icons-material/EmojiEvents';
const Awards = () => {
  return (
    <div>
    <div className='p-7 pb-0'>
        <p className='p-8 text-4xl' style={{width:"70%"}}>
        We won <span className='font-bold'> 30+ design awards in 2022</span> stand alone, some of which had us share the stage with Google, Netflix, and Spotify.
        </p>

        <div class="grid grid-cols-6 grid-rows-3 gap-2 p-8 pb-0">
    <span className='' style={{  background:"#f5f5f5",   borderRadius: "50%" ,padding: "51px"}}><EmojiEventsIcon style={{fontSize:"82px" ,color:"black"}} /> </span>
    <span className='' style={{  background:"#f5f5f5",   borderRadius: "50%" ,padding: "51px"}}><EmojiEventsIcon style={{fontSize:"82px" ,color:"black"}} /></span>
    <span className='' style={{  background:"#f5f5f5",   borderRadius: "50%" ,padding: "51px"}}><EmojiEventsIcon style={{fontSize:"82px" ,color:"black"}} /></span>
    <span className='' style={{  background:"#f5f5f5",   borderRadius: "50%" ,padding: "51px"}}><EmojiEventsIcon style={{fontSize:"82px" ,color:"black"}} /></span>
    <span className='' style={{  background:"#f5f5f5",   borderRadius: "50%" ,padding: "51px"}}><EmojiEventsIcon style={{fontSize:"82px" ,color:"black"}} /></span>
    <span className='' style={{  background:"#f5f5f5",   borderRadius: "50%" ,padding: "51px"}}><EmojiEventsIcon style={{fontSize:"82px" ,color:"black"}} /></span>
    <span className='' style={{  background:"#f5f5f5",   borderRadius: "50%" ,padding: "51px"}}><EmojiEventsIcon style={{fontSize:"82px" ,color:"black"}} /></span>
    <span className='' style={{  background:"#f5f5f5",   borderRadius: "50%" ,padding: "51px"}}><EmojiEventsIcon style={{fontSize:"82px" ,color:"black"}} /></span>
    <span className='' style={{  background:"#f5f5f5",   borderRadius: "50%" ,padding: "51px"}}><EmojiEventsIcon style={{fontSize:"82px" ,color:"black"}} /></span>
    <span className='' style={{  background:"#f5f5f5",   borderRadius: "50%" ,padding: "51px"}}><EmojiEventsIcon style={{fontSize:"82px" ,color:"black"}} /></span>
    <span className='' style={{  background:"#f5f5f5",   borderRadius: "50%" ,padding: "51px"}}><EmojiEventsIcon style={{fontSize:"82px" ,color:"black"}} /></span>
    <span className='' style={{  background:"#f5f5f5",   borderRadius: "50%" ,padding: "51px"}}><EmojiEventsIcon style={{fontSize:"82px" ,color:"black"}} /></span>
  </div>
        </div>

    <div className='pr-5 pl-5'><hr  /></div>   

    </div>
  )
}

export default Awards