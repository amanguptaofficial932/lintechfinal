import React from 'react'

const Hero = () => {
  return (
   <div>
  {/* Component Code */}
  <div className="relative h-screen w-full flex items-center justify-center text-center bg-cover bg-center" style={{background:"#151515"}}>
    <div className="absolute top-0 " />
    <main className="px-4 sm:px-6 lg:px-8 z-10">
      <div className="text-center">
        <h1 className="text-4xl tracking-tight leading-10  sm:text-5xl font-bold text-white sm:leading-none md:text-8xl">
    DIGITAL PRODUCT
        </h1>
        <p className="text-white text-8xl font-bold">
      DEVELOPMENT
        </p>
       
        <p className='text-white m-auto mt-10 text-1xl' style={{width:"80%" ,fontSize:"18px" }}>We collaborate with businesses of all sizes, from startups to established enterprises, to craft captivating digital solutions that align with their objectives by bridging the gap between user needs and business goals.</p>
         
          
      
      </div>
      <div className="">
      
      </div>
      
      <div class="grid grid-cols-4 grid-rows-3 gap-2 mt-8">
    
      <span className=" text-white">WE’VE BUILT <br /> SOLUTIONS FOR</span>
      <img src="https://lintechgroup.co.uk/img/uploads/clientele_image/16831983331893246536.png?w=120&fit=max&fm=webp" alt="Versace" />
      <img src="https://lintechgroup.co.uk/img/uploads/clientele_image/16831984761384573791.png?w=120&fit=max&fm=webp" alt="KPMG" />
      <img src="https://lintechgroup.co.uk/img/uploads/clientele_image/16831985181380827421.png?w=120&fit=max&fm=webp" alt="Remax" />


  </div>
   <div className="grid  gap-4">

  
  
 

</div>


    </main>
  </div>

</div>

  )
}

export default Hero