'use client'
import React from 'react'
import Link from 'next/link';

import WebIcon from '@mui/icons-material/Web';


const NavBar = () => {
    const [open, setOpen] = React.useState(false);
    const [flyer, setFlyer] = React.useState(false);
    const [flyerTwo, setFlyerTwo] = React.useState(false);
  
    return (
      <>
        {/* This example requires Tailwind CSS v2.0+ */}
        <div className=" p-9 " style={{background:"#151515"}}>
          <div className="max-w-7xl mx-auto px-4 sm:px-6">
            <div className="flex justify-between items-center  md:justify-start md:space-x-10">
              <div className="flex">
          <Link href="/"  className='text-white font-bold text-lg'>LINTECH GROUP.</Link>
          
                 
               
              </div>
              <div className="-mr-2 -my-2 md:hidden">
                <button
                  type="button"
                  className="bg-white rounded-md p-2 inline-flex items-center justify-center text-gray-400 hover:text-gray-500 hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-indigo-500"
                  onClick={() => setOpen(!open)}
                >
                  <span className="sr-only">Open menu</span>
                  {/* Heroicon name: outline/menu */}
                  <svg
                    className="h-6 w-6"
                    xmlns="http://www.w3.org/2000/svg"
                    fill="none"
                    viewBox="0 0 24 24"
                    stroke="currentColor"
                    aria-hidden="true"
                  >
                    <path
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      strokeWidth={2}
                      d="M4 6h16M4 12h16M4 18h16"
                    />
                  </svg>
                </button>
              </div>
              <nav className="hidden md:flex ">
                <div className="relative">
                  {/* Item active: "text-gray-900", Item inactive: "text-gray-500" */}
                  <a
                  href="#"
                  className="text-white font-medium  hover:text-gray mx-3"
                >
                  Home
                </a>
                
                  <button
                    type="button"
                    className="
                     group  rounded-md text-gray-500 inline-flex items-center text-base font-medium hover:text-gray-900 focus:outline-none  pb-8'
                    "
                    onClick={() => (setFlyer(!flyer), setFlyerTwo(false))}
                  >
                    <span className='text-white'>Services</span>
                
                    <svg
                      className={
                        flyer === true
                          ? "transform rotate-180 ml-2 h-5 w-5 text-gray-400  group-hover:text-gray-500 transition ease-out duration-200"
                          : "transform rotate-0 transition ease-out duration-200 ml-2 h-5 w-5 text-gray-400 group-hover:text-gray-500"
                      }
                      xmlns="http://www.w3.org/2000/svg"
                      viewBox="0 0 20 20"
                      fill="currentColor"
                      aria-hidden="true"
                    >
                      <path
                        fillRule="evenodd"
                        d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z"
                        clipRule="evenodd"
                      />
                    </svg>
                  </button>
           
                  <div
                    onMouseLeave={() => setFlyer(false)}
                    className={
                      flyer
                        ? " opacity-100 translate-y-0 z-40 transition ease-out duration-200 absolute  -ml-4 mt-3 transform px-2 w-screen max-w-md sm:px-0 lg:ml-0 lg:left-1/2 lg:-translate-x-1/2"
                        : " opacity-0 translate-y-1 absolute z-10 -ml-4 mt-3 transform px-2 w-screen max-w-md sm:px-0 lg:ml-0 lg:left-1/2 lg:-translate-x-1/2"
                    }
                  >
                    <div className="rounded-lg shadow-lg overflow-hidden" >
                      
                      
                    <div className="relative grid grid-cols-3 grid-rows-1 gap-6 px-5 py-6 sm:gap-8 sm:p-8" style={{background:"#333"}}>
                      
                          
                            <div className='' style={{width:"180px"}}>
                                <h3 className='text-white'>Development</h3>
                                <ul className='text-muted'>
                                    <li className='cursor-pointer p-1 mt-3 rounded-md hover:text-white hover:bg-black' >
                                    <WebIcon className='text-white'/>  Web Development
                                    </li>
                                    <li className='cursor-pointer p-1 mt-3 rounded-md hover:text-white hover:bg-black' >
                                    <WebIcon className='text-white'/>  Web design
                                    </li>
                                    <li className='cursor-pointer p-1 mt-3 rounded-md hover:text-white hover:bg-black' >
                                    <WebIcon className='text-white'/>  e-commerce
                                    </li>
                                    <li className='cursor-pointer p-1 mt-3 rounded-md hover:text-white hover:bg-black' >
                                    <WebIcon className='text-white'/>  mobile-app
                                    </li>
                                    
                                    <li className='cursor-pointer p-1 mt-3 rounded-md hover:text-white hover:bg-black' >
                                    <WebIcon className='text-white'/>  sass development
                                    </li>
                                </ul>
                            </div>
                            <div className='pl-10' style={{width:"280px"}}>
                               <h3 className='text-white '>Marketing</h3>
                               <ul className='text-muted'>
                                    <li className='cursor-pointer p-1 mt-3 rounded-md hover:text-white hover:bg-black' >
                                    <WebIcon className='text-white'/>  Web Development
                                    </li>
                                    <li className='cursor-pointer p-1 mt-3 rounded-md hover:text-white hover:bg-black' >
                                    <WebIcon className='text-white'/>  Web design
                                    </li>
                                    <li className='cursor-pointer p-1 mt-3 rounded-md hover:text-white hover:bg-black' >
                                    <WebIcon className='text-white'/>  e-commerce
                                    </li>
                                    <li className='cursor-pointer p-1 mt-3 rounded-md hover:text-white hover:bg-black' >
                                    <WebIcon className='text-white'/>  mobile-app
                                    </li>
                                    
                                    <li className='cursor-pointer p-1 mt-3 rounded-md hover:text-white hover:bg-black' >
                                    <WebIcon className='text-white'/>  sass development
                                    </li>
                                </ul>
                            </div>
                            
                        
                  
                       
                       
                      </div>
                    </div>
                  </div>
                </div>
  
                <a
                  href="#"
                  className="text-white font-medium  hover:text-gray mx-3"
                >
                  PROCESS
                </a>
                <a
                  href="#"
                  className="text-white font-medium  hover:text-gray mx-3"
                >
                  ABOUT
                </a>
                <a
                  href="#"
                  className="text-white font-medium  hover:text-gray mx-3"
                >
                 CAREERS
                </a>
                <a
                  href="#"
                  className="text-white font-medium  hover:text-gray mx-3"
                >
                 CONTACT
                </a>
                <a
                  href="#"
                  className="text-white font-medium  hover:text-gray mx-3"
                >
                 NEWS
                </a>
                <a
                  href="#"
                  className="text-white font-medium  hover:text-gray mx-3"
                >
                 TESTIMONIALS
                </a>
               
              </nav>
            
            </div>
          </div>
          {/*
      Mobile menu, show/hide based on mobile menu state.
  
      Entering: "duration-200 ease-out"
        From: ""
        To: ""
      Leaving: "duration-100 ease-in"
        From: "opacity-100 scale-100"
        To: "opacity-0 scale-95"
    */}
  
         
        </div>
      </>
    );
  };

  export default NavBar
  