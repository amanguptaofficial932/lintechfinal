import Image from 'next/image'
import Header from './components/Header'
import Navbar from './components/Navbar'
import Hero from './components/Hero'
import Awards from './components/Awards'
import Card from './components/Card'
import Content from './components/Content'
export default function Home() {
  return (
   <div>
   <Navbar/>
  <Hero/>
  <Awards/>
  <Card/>
  <Content/>
   </div>
  )
}
